<%--
/**
* Copyright (c) 2000-2010 Liferay, Inc. All rights reserved.
*
* This library is free software; you can redistribute it and/or modify it under
* the terms of the GNU Lesser General Public License as published by the Free
* Software Foundation; either version 2.1 of the License, or (at your option)
* any later version.
*
* This library is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
* details.
*/
--%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<div class="bigBgCenter">
	<div class="hRCareDoc">
		<div class="videoBox"></div>
		<div class="hRCareDocBox">
			<div class="regPatientImg"></div>
			<div class="regDocText"> Patient Registration     Free </div>
		</div>
	</div>
	<div class="ourImgBox">
		<div class="hairSolutions">
			<div class="EffectivImg">
				<div class="EffectivImgText">Successful Hair Solutions</div>
			</div>
		</div>
		<div class="cosmeticSolutions">
			<div class="EffectivImg">
				<div class="EffectivImgText">Successful Cosmetic Solutions</div>
			</div>
		</div>
		<div class="ayurvedicSolutions">
			<div class="EffectivImg">
				<div class="EffectivImgText">Successful Ayurvedic Solutions</div>
			</div>
		</div>
		<div class="dentalSolutions">
			<div class="EffectivImg">
				<div class="EffectivImgText">Successful Dental Solutions</div>
			</div>
		</div>
	</div>
	<div class="regPatientlbox">
		<div style="color:#FFF;">
			<p>*=Required field</p>
			<p>Contact Infromation</p>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Full Name:</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Gender:</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<select class="fieldIpu" name=""  > <option > </option></select>
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">email ID</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Country</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Zip or pin code</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Phone No:<br /></div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Address:</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox" style="color:#FFF; font-family:Arial, aa smooth; font-size:11px; margin-left:215px;">
			By signing up, you agree to our Terms <br />Read our Privacy Policy
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Create a User ID</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Password:</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regFieldFirstName">
				<div class="regFieldText">Confirm Password:</div>
				<div style="width:180px; float:right; margin-top:5px;">
					<input class="fieldIpu" name="" />
				</div>
			</div>
		</div>
		<div class="regPatientFieldBox">
			<div class="regPatientSubmitBtn">
				<div class="regPatientSubmitBtnText">SUBMIT</div>
			</div>
		</div>
	</div>
</div>
